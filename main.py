from pdb import Restart
from select import select
import sys, random
from turtle import position
from unicodedata import category
import pygame
from random import randint 

class Game:
    #variables + fonctions

    def __init__(self):
        # resolution de la fenetre pygame
        self.screen = pygame.display.set_mode((800, 800))

        # Gerer le nom de la fenetre pygame
        pygame.display.set_caption('Snake')

        self.game_inprogress = True

        # Position du snake
        self.snake_position_x = 300
        self.snake_position_y = 300
        self.snake_direction_x = 0
        self.snake_direction_y = 0

        self.last_direction = None
        # Taille du snake
        self.snake_size = 10

        self.apple = Apple(randint(1,4))
        # Position de la pomme
        # self.apple.position_x = random.randrange(110, 690, 10)
        # self.apple.position_y = random.randrange(110, 690, 10)
        # Taille de la pomme
        # self.apple_size = 10

        self.clock = pygame.time.Clock()

        self.difficulty = 10

        # Position de toutes les parties du snake
        self.snake_position = []

        self.full_snake = 1

        self.score = 0

        self.scores = []

        self.high_score = 5

        self.menu_screen = True
        self.menu_difficulty_screen = False

        # self.get_scores()

        self.img_shenron = pygame.image.load('shenron.png')

        self.img_body = pygame.image.load('body.png')

        # self.img_cristal = pygame.image.load('cristal.png')

        self.beginning_screen = True

    def get_scores(self):
        self.scores = []
        with open('scores.txt', 'r') as file:
            for line in file:
                self.scores.append(line.strip())

        return self.scores

    def set_scores(self, sort_scores):
        with open('scores.txt', 'w') as file:
            for score in sort_scores:
                file.write(str(score)+'\n')

    def new_highscore(self):
        self.scores.append(str(self.full_snake))
        self.scores.sort()
        self.scores = self.scores[1:]
        int_scores = [int(x) for x in self.scores]
        int_scores.sort(reverse=True)
        self.set_scores(int_scores)

    def check_scores(self): 
        for score in self.scores:
            if self.full_snake > int(score):
                self.new_highscore()
                break

    def main_function(self):
        
        while self.menu_screen:
            if self.menu_difficulty_screen:
                self.show_difficulty_menu()
            else:
                self.show_menu()


        while self.game_inprogress:
           
            for evenement in pygame.event.get():
                if evenement.type == pygame.QUIT:
                    sys.exit()

                if evenement.type == pygame.USEREVENT:
                    # Timer expired. on refresh la pomme
                    self.apple.refresh()
                    self.display_element()

                # Gestion des mouvement du snake
                if evenement.type == pygame.KEYDOWN:

                    if evenement.key == pygame.K_RIGHT:
                        if(self.last_direction is None or self.last_direction !=  pygame.K_LEFT):
                            self.snake_direction_x = 10
                            self.snake_direction_y = 0
                            self.last_direction = pygame.K_RIGHT

                    if evenement.key == pygame.K_LEFT:
                        if(self.last_direction is None or self.last_direction !=  pygame.K_RIGHT):
                            self.snake_direction_x = -10
                            self.snake_direction_y = 0
                            self.last_direction = pygame.K_LEFT

                    if evenement.key == pygame.K_UP:
                        if(self.last_direction is None or self.last_direction !=  pygame.K_DOWN):
                            self.snake_direction_x = 0
                            self.snake_direction_y = -10
                            self.last_direction = pygame.K_UP

                    if evenement.key == pygame.K_DOWN:
                        if(self.last_direction is None or self.last_direction !=  pygame.K_UP):
                            self.snake_direction_x = 0
                            self.snake_direction_y = 10
                            self.last_direction = pygame.K_DOWN

                    if evenement.key == pygame.K_r:
                        self.game_inprogress = False
                        self.restart()


            if self.snake_position_x <= 50 or self.snake_position_x >= 750 or self.snake_position_y <= 50 or self.snake_position_y >= 750 :
                # afficher le menu game over 
                self.check_scores()
                self.game_inprogress = False
                self.show_gameover_menu()
                # ajouter un menu game over, snake border touch
                #récupere le score
                

            self.snake_move()

            # Evenement "snake mange la pomme"
            if self.apple.position_y == self.snake_position_y and self.apple.position_x == self.snake_position_x:          
                # Augmenter la taille du snake mange
                if (self.full_snake + self.apple.point) < 0:
                    self.game_inprogress = False
                    self.show_gameover_menu()
                else:
                    if self.apple.point < 0:
                        for i in range(self.apple.point):
                            del self.snake_position[-1]
                    self.full_snake += self.apple.point
                    self.score = 0 if (self.score + self.apple.point) < 0 else self.score + self.apple.point
                    self.apple.refresh()
              
                
            

            # Historique position tete du snake
            snake_head = []
            snake_head.append(self.snake_position_x)
            snake_head.append(self.snake_position_y)

            self.snake_position.append(snake_head)

            if len(self.snake_position) > self.full_snake:

                self.snake_position.pop(0)
            
            self.display_element()
            self.eat_self(snake_head)

            self.create_message("big", '{}'.format(str(self.score)), (375, 10, 50, 50), (255, 255, 255))
            self.create_message("big", '{}'.format('press r to restart'), (10, 10, 50, 50), (255, 255, 255))


            # affichage des limites
            self.game_limit()
        
            self.clock.tick(self.difficulty)

            # affichage du screen
            pygame.display.flip()

    # creation des limites de l'arene de jeu
    def game_limit(self):
        pygame.draw.rect(self.screen,(255,255,255),(50,50,700,700),5)
    
    # Mouvement du snake
    def snake_move(self):
        self.snake_position_x += self.snake_direction_x
        self.snake_position_y += self.snake_direction_y
    
    # Afficher les element du jeux
    def display_element(self):
        self.screen.fill((0,0,0))

        # affichage du snake
        # pygame.draw.rect(self.screen,(0, 255, 0),(self.snake_position_x, self.snake_position_y, self.snake_size, self.snake_size))

        self.screen.blit(self.img_shenron,(self.snake_position_x, self.snake_position_y, self.snake_size, self.snake_size))

        # affichage de la pomme
        self.display_apple()

        # affichage corps du snake
        self.display_snake()

    def display_apple(self):
        self.screen.blit(self.apple.image, (self.apple.position_x, self.apple.position_y, self.apple.size, self.apple.size))


    # Afficher trainer du serpent
    def display_snake(self):
        for snake_body in self.snake_position[:-1]:
            self.screen.blit(self.img_body, (snake_body[0], snake_body[1], self.snake_size, self.snake_size))
    
    def eat_self(self, snake_head):
        for snake_body in self.snake_position[:-1]:

            if snake_head == snake_body:
                self.check_scores()
                self.game_inprogress = False
                #recuperer le score

    def create_message(self, font, message, message_rectangle, color):
        if font == 'small':
            font = pygame.font.SysFont('Lato',20,False)
        elif font == 'big':
            font = pygame.font.SysFont('Lato',40,True)

        message = font.render(message, True, color)

        self.screen.blit(message, message_rectangle)
    


    def show_menu(self):
        
        for evenement in pygame.event.get():
            if evenement.type == pygame.QUIT:
                sys.exit()
            
            elif evenement.type == pygame.KEYDOWN:
                if evenement.key == pygame.K_RETURN:
                    self.menu_difficulty_screen = True
        
        # on récupère les scores enregistrés
        scores = self.get_scores()

        self.screen.fill((0,0,0))
        self.create_message('big','Snake',(300,300,100,50),(255,255,255))
        self.create_message('big','{}'.format(str(scores)),(200,450,100,50),(255,255,255))
        pygame.display.flip()
        

    def show_difficulty_menu(self):
        
      
        for evenement in pygame.event.get():
            if evenement.type == pygame.QUIT:
                sys.exit()
            
            elif evenement.type == pygame.KEYDOWN:
                if evenement.key == pygame.K_0:
                    self.difficulty = 10
                elif evenement.key == pygame.K_1:
                    self.difficulty = 30
                elif evenement.key == pygame.K_2:
                    self.difficulty = 60
                
                self.menu_screen = False
       
        self.screen.fill((0,0,0))
        self.create_message('big','',(300,300,100,50),(255,255,255))
        self.create_message('small','easy: press 0',(300,200,100,50),(255,255,255))
        self.create_message('small','normal: press 1',(300,300,100,50),(255,255,255))
        self.create_message('small','insame: press 2',(300,400,100,50),(255,255,255))
        pygame.display.update()

        # self.menu_screen = True
        # self.game_inprogress = False
        # self.main_function()

    def show_gameover_menu(self):

        self.screen.fill((0,0,0))
        self.create_message('big','Game Over',(300,300,100,50),(255,255,255))
        self.create_message('big','Press R to restart',(300,400,100,50),(255,255,255))
        pygame.display.update()

        while not self.game_inprogress:
           
            for evenement in pygame.event.get():
                if evenement.type == pygame.QUIT:
                    sys.exit()
                
                if evenement.type == pygame.KEYDOWN:
                    if evenement.key == pygame.K_r:
                        self.restart()
                        # self.menu_screen = True
                    elif evenement.key == pygame.K_RETURN:
                        sys.exit()

            

        # sys.exit()

    def restart(self):
        pygame.event.clear()
        # Position du snake
        self.snake_position_x = 300
        self.snake_position_y = 300
        self.snake_direction_x = 0
        self.snake_direction_y = 0

        # Taille du snake
        self.snake_size = 10

        # Position de la pomme
        self.apple.position_x = random.randrange(110, 690, 10)
        self.apple.position_y = random.randrange(110, 690, 10)

        self.score = 0

        self.game_inprogress = True
        self.menu_screen = True
        self.menu_difficulty_screen = True

        self.snake_position = []

        self.apple.refresh()

        self.main_function()


class Apple:
    def __init__(self, category):

      
        self.categories_img = {1:'cristal.png', 2:'cristal1.png', 3:'cristal2.png', 4:'cristal3.png'}
        self.categories_point = {1:1, 2:2, 3:3, 4:4}

        self.categorie = category
        self.image = self.categories_img.get(category) 
        self.point = category 

     

        # Position de la pomme
        self.position_x = random.randrange(110, 690, 10)
        self.position_y = random.randrange(110, 690, 10)
      
        # Taille de la pomme
        self.size = 10

        # self.img = pygame.image.load(dict.get(key, default = None)) 

    @property	  	 	 		 		    	 
    def category(self):	  	 	 		 		    	 
        return self._category	  	 	 		 		    	 
	  	 	 		 		    	 
    @category.setter	  	 	 		 		    	 
    def category(self, category):	  	 	 		 		    	 
        try:	  	 	 		 		    	 
            self._category = category                 		 		    	 
        except Exception:	  	 	 		 		    	 
            raise AttributeError

    @property	  	 	 		 		    	 
    def image(self):	  	 	 		 		    	 
        return self._image 	 	 		 		    	 
	  	 	 		 		    	 
    @image.setter	  	 	 		 		    	 
    def image(self, path):	  	 	 		 		    	 
        try: 	 	 		 		    	 
            self._image = pygame.image.load(path) 		 		    	 
        except Exception:	  	 	 		 		    	 
            raise AttributeError

    @property	  	 	 		 		    	 
    def point(self):	  	 	 		 		    	 
        return self._point 	 	 		 		    	 
	  	 	 		 		    	 
    @point.setter	  	 	 		 		    	 
    def point(self, category):	  	 	 		 		    	 
        try:	  	 	 		 		    	 
            self._point = self.categories_point.get(category)	 		 		    	 
        except Exception:	  	 	 		 		    	 
            raise AttributeError

    @property	  	 	 		 		    	 
    def position_y(self):	  	 	 		 		    	 
        return self._position_y 	 	 		 		    	 
	  	 	 		 		    	 
    @position_y.setter	  	 	 		 		    	 
    def position_y(self, position):	  	 	 		 		    	 
        try:	  	 	 		 		    	 
            self._position_y = position
	 	 		 		    	 
        except Exception:	  	 	 		 		    	 
            raise AttributeError
    
    @property	  	 	 		 		    	 
    def position_x(self):	  	 	 		 		    	 
        return self._position_x	 	 		 		    	 
	  	 	 		 		    	 
    @position_x.setter	  	 	 		 		    	 
    def position_x(self, position):	  	 	 		 		    	 
        try:	  	 	 		 		    	 
            self._position_x= position
	 	 		 		    	 
        except Exception:	  	 	 		 		    	 
            raise AttributeError

    def refresh_positions(self):
        self.position_x = random.randrange(110, 690, 10)
        self.position_y = random.randrange(110, 690, 10)
    
    def refresh(self):
        category = randint(1,4)
        self.category = category
        self.image = self.categories_img.get(category) 
        self.point = category 
        pygame.time.set_timer(pygame.USEREVENT, 10000)
        self.refresh_positions()

if __name__ == '__main__':

    pygame.init()
    Game().main_function()
    pygame.quit()